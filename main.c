#include <stdio.h>
#include <strings.h>
#include <stddef.h>
#include <stdbool.h>

int
main(int argc, char **argv) {
	if (argc != 2) {
		perror("ERROR: you must specify the [PATH] of your drive.");
		return -1;
	}

	FILE *file;
	file = fopen(argv[1], "w");
	if (file == NULL) {
		perror("ERROR: could not open the file.");
		return -1;
	}
	
	char buff[8192];	
	bzero(buff, 8192);

	size_t bytes;
	bytes = 0;

	size_t count;
	while (true) {
		count  = fwrite(buff, sizeof (buff), sizeof (char), file);
		bytes += count;
		if (count != sizeof (buff)) {
			perror("ERROR: could not write to the file.");
			break;
		}
	}

	printf("INFO: wrote %zu bytes.", bytes);

	fclose(file);

	return 0;
}
