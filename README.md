# 0xDD

zer0s out the given drive.

### How to use it

- first you need to build it by typeing `make`.
- then you have to specify the path of your drive (e.g. `/dev/sdb`).

```
0xDD /dev/sdb
```
